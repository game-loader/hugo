---
title: "我的朋友们"
subtitle: ""
type: "friends"
date: 2022-04-10T21:35:38+08:00
description: "Logic's friends"
keywords: 
  - Hugo
  - friends tempalate
comment: false
---

<!-- When you set data `friends.yml` in `yourProject/data/` directory, it will be automatically loaded here. -->
---
<!-- You can define additional content below for this page. -->
## Base info

```yaml
blank now  ^_^
```

## Friendly Reminder

{{< admonition info "Notice" true >}}
1. If you want to exchange link, please leave a comment in the above format. (personal non-commercial blogs / websites only)
2. Warn: Website failure, stop maintenance and improper content may be cancelled!
3. Those websites that do not respect other people's labor achievements, reprint without source, or malicious acts, please do not come to exchange.
{{< /admonition >}}
