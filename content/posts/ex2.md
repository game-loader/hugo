+++
title = "Ex2"
date = 2022-03-23
categories = ["网络安全实验"]
draft = false
author = "Logic"
+++

## 实验过程 {#实验过程}


### 在不同的操作系统环境下安装和配置OSSEC代理，构建入侵检测环境 {#在不同的操作系统环境下安装和配置ossec代理-构建入侵检测环境}


#### 在win平台下安装配置OSSEC {#在win平台下安装配置ossec}

在C盘tools文件夹下找到OSSEC安装包并安装

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/03238ffYqP.png" >}}

输入OSSEC服务器地址并保存

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/0323sDC3iO.png" >}}

使用putty远程登陆OSSIM服务器

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/0323aa2XZF.png" >}}

启动OSSEC代理管理程序

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/0323ABExuX.png" >}}

创建代理并生成密钥

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/03230MF1k8.png" >}}

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/0323jz7c0u.png" >}}

将密钥输入OSSEC AGENT管理器窗口，保存

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/03230qvYU6.png" >}}

重启服务器的OSSEC程序

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/0323KU8Id5.png" >}}

查看OSSEC AGENT状态信息

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/0323kx2LUt.png" >}}

在服务端查看AGENT状态，发现为never connected

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/0323X7htCV.png" >}}

在windows查看ossec的log信息，发现如下：

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/0323hE3gfE.png" >}}

可见服务端不知为什么没有回应。服务端查看log信息看不到windows的连接请求。经过排查，发现原来是二者的请求队列没有同步。在windows端有如下配置：

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/0323Dqxvfu.png" >}}

在服务端查看/var/ossec/queue/rids/sender_counter可知

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/0323EWowem.png" >}}

二者请求队列不同步，删掉服务端的sender_counter，重启服务端和windows客户端，再次查看，windows客户端变为active

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/0323P6U2JF.png" >}}

此时windows端已变为Active(序号为008是删除后重新添加了两次)


#### 在Linux平台下安装和配置OSSEC代理 {#在linux平台下安装和配置ossec代理}

登陆到Centos虚拟机，使用SSH远程登陆到OSSIM服务器

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/0323JZKzfY.png" >}}

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/0323lvJMRK.png" >}}

打开OSSEC代理管理器程序，新建一个代理，生成并添加密钥。

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/0323F9lJCX.png" >}}

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/0323Xoj1vy.png" >}}

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/0323BNpNFz.png" >}}

ossec.conf在/var/ossec/etc目录下，查看其内容可以看到

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/0323NmkViT.png" >}}

已经存在，故直接保存，重启服务端，启动代理端

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/0323z7s4bL.png" >}}

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/0323DsWuZU.png" >}}

在服务端查看客户端状态

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/0323O2dAMj.png" >}}

发现Centos和windows都是never connected状态，我们查看ossec的log文件信息。可发现如下错误：

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/0323C4sjXb.png" >}}

缺少agent.conf文件，我们创建这个文件，重启客户端。再次在服务端查看客户端状态

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/0323X7t7qh.png" >}}

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/0323stizxU.png" >}}

可见状态为Active，已经成功上线。


### 监视OSSIM服务器本地root用户的登录情况 {#监视ossim服务器本地root用户的登录情况}


#### 登陆OSSIM web gui {#登陆ossim-web-gui}

打开火狐，输入192.168.1.200，默认进入登陆页面，输入用户名和密码登陆成功

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/0323Mm6Owx.png" >}}

单击Analysis---&gt;Detection---&gt;HIDS---&gt;Config---&gt;Ossec.conf，可以看到OSSIM集成检测平台已经默认监视了日志文件/var/log/auth.log

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/0323liCdiQ.png" >}}

重启OSSIM服务器，重启登录成功后进入图形操作界面，按Ctrl+Alt+F1切换到命令行界面，输入用户名root和密码Simplexue123进行登录，再输入命令exit退出登录，之后按Ctrl+Alt+F7回到图形界面。

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/0323fioHji.png" >}}

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/0323ikk5p4.png" >}}

在windows2012上远程连接到服务器192.168.1.200

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/0323cK4xOh.png" >}}

在windows2012的OSSIM Web页面上，单击Analysis---&gt; Security Events (SIEM)，可以看到，Security Events页面中列出了OSSIM系统预设检测规则适用范围内的所有安全事件日志信息，可以找到通过putty远程登录时相关的SSH登录记录报警信息。

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/03230z7fmO.png" >}}

OSSIM监测到的SShd登录成功的报警日志的signature信息为

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/0323lpcx9H.png" >}}

在OSSIM web页面搜索框输入ossec，回车进行ossec报警数据过滤

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/0323rixBKZ.png" >}}


### 基于SSH的远程非法入侵检测 {#基于ssh的远程非法入侵检测}

使用putty工具远程登录OSSIM服务器，在打开的终端中，使用CD命令进入“/var/ossec/rules”目录（该目录为OSSEC服务器的检测规则文件存储目录），并使用ls命令查看所有的OSSEC服务器端检测规则文件。

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/0323cKMDTa.png" >}}

使用vim打开sshd_rules.xml规则文件,找到id为5719的规则，将level级别设置为2，该规则表示：当非法用户存在2次以上远程登录尝试操作，且操作时间超过30秒，那么将触发非法远程登录尝试报警。修改完sshd_rules.xml文件后保存并退出vim。需注意的是，这里的非法用户denied user指的是系统中存在但是被禁止通过ssh远程登陆系统的用户.

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/03231dakyr.png" >}}

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/0323MkboxN.png" >}}

重新启动ossec服务器，以使sshd_rules.xml文件配置生效。

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/0323xzGQaj.png" >}}

通过putty连接到OSSIM服务器，创建一个新用户deny

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/0323pXDLem.png" >}}

配置ssh拒绝该用户登陆

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/03232ugcxU.png" >}}

使用该用户多次尝试登陆服务器

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/03236V7pkP.png" >}}

在ossim web端，输入ossec进行ossec报警信息筛选，可以看到5719规则对应的报警信息

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/0323qORvol.png" >}}


### 监视CentOS7 root用户情况 {#监视centos7-root用户情况}

在OSSIM集成检测平台上设置规则，监测CentOS7用户情况。在CentOS7终端查看代理的配置文件，可以看到OSSIM集成检测平台默认监控/var/log/secure文件

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/0323veoYmU.png" >}}

重启OSSIM服务器（192.168.1.200），使用putty登陆到CentOS7

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/0323CY8pWR.png" >}}

添加新用户simpleware，并将其密码设为Simplexue123

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/0323h5eE1i.png" >}}

回到OSSIM Web页面上，进行OSSEC警报数据的过滤，可以看到与CentOS7添加新用户相关的OSSEC报警信息。

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/0323tU8QGv.png" >}}


### 监控Web服务器的访问日志 {#监控web服务器的访问日志}

用vim打开CentOS7的ossec.conf文件，添加如下内容

> &lt;localfile&gt;
>         &lt;log_format&gt;apache&lt;/log_format&gt;
>         &lt;location&gt;/var/log/httpd/access_log&lt;/location&gt;
>     &lt;/localfile&gt;

保存文件

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/0323fFZHYH.png" >}}

重新启动OSSEC服务

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/0323d5FyJC.png" >}}

在windows2012（IP为192.168.1.5）的火狐浏览器上新打开一个页面，访问 `http://192.168.1.6/dvwa/config`

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/032379ET2x.png" >}}

回到OSSIM Web页面上，进行OSSEC警报数据的过滤，可以看到访问禁止目录时的报警信息。

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/0323GuGVDp.png" >}}
