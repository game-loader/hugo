+++
title = "bpf minimal c 分析"
author = "Logic"
date = 2022-03-08
draft = false
+++

## minimal C 源码分析 {#minimal-c-源码分析}


### libbpf\_set\_strict\_mode {#libbpf-set-strict-mode}

这个函数的作用乍一看让人摸不着头脑，查看Andrii Nakryiko的博客<font color="blue">[Building BPF applications with libbpf-bootstrap](https://nakryiko.com/posts/libbpf-bootstrap/#minimal-app)</font>可发现在该篇博文中出现的用于解除用户内存限制的 `bump_memlock_rlimit()` 函数并没有出现最新的minimal.c文件中，新增的函数即 `libbpf_set_strict_mode()` ，可推测该函数起到了之前bump函数的作用。但对于该API的说明在源码中并没有给出相关的注释，查看libbpf github上release记录，查找有关的变化说明，最终可以找到<font color="blue">[Libbpf 1.0 migration guide](https://github.com/libbpf/libbpf/wiki/Libbpf-1.0-migration-guide)</font>,其中，对该API给出了说明。可见该API的的作用在于启动libbpf 1.0的新特性，在最新的release v0.7中说明了有关特性，即

> no need for explicit setrlimi(RLIMIT\_MEMLOCK) when LIBBPF\_STRICT\_AUTO\_RLIMIT\_MEMLOCK is passed to libbpf\_set\_strict\_mode(). libbpf will determine whether this is necessary automatically based on kernel's support for memcg-based memory accounting for BPF;

libbpf会自动根据需求和内核支持来进行相应的内存变化,这一特性实在是太新了，所以搜索不到任何说明，只能跟踪源码仓库变化了解。


### 其余部分 {#其余部分}

其余部分的解析在Andrii的<font color="blue">[博客](https://nakryiko.com/posts/libbpf-bootstrap/)</font>中有非常详细的说明,在此不再赘述，这里主要再讨论一下这几个文件之间的关系，minimal.bpf.c其实是bpf程序的主体文件，在其中定义的全局变量即是bpf程序中的map表，用于bpf程序内的数据交互和监控。该变量在通过bpftool生成skel文件时会被放入minimal\_bpf这一结构体的bss结构中，相当于放入elf文件的数据段中，这样minimal.c文件就可以通过读取该结构中的数据来将一些数据从用户空间传入内核bpf结构中。minimal.c则是用户想要进行的一些处理，比如打印某些信息或将信息进一步传递。
