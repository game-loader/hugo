---
title: 工具经验手册
author: Logic
date: 2024-09-19
categories: [""]
tags: []
draft: false
---

## 快捷键设定

阅读[kitty文档](https://sw.kovidgoyal.net/kitty/actions/)可知可以通过map将某个快捷键绑定为一个或者一系列文档中的actions。如果要执行多个actions可以使用combine，在这里我们将cmd+e绑定为打开一个新的kitty tab标签页并执行ssh \*\*<Tab>来使用fzf的ssh补全展开所有的ssh hostname，利用fzf来选择要连接的host. 配置文件中的内容如下

```conf
# kitty.conf

# 设置新标签页的快捷键为 Cmd+E
map cmd+e combine | new_tab | send_text normal,application ssh **\t

```
