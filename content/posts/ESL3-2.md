---
title: ESL3.2 笔记
author: Logic
date: 2023-10-11
categories: ["笔记"]
tags: ["ESL"]
draft: false
---

## 3.2.2 高斯-马尔可夫定理

## 3.2.3 用单变量回归理解多元线性回归模型

对于单变量来说，输入数据可以用向量$\mathbf{x}=(x_1,x_2...x_N)^T$来表示，其中$x_j$代表第j个输入数据。每个输入数据只有一个变量x，故可以用一个标量来表示。设每个$x_j$对应的观测值（也就是实际的值）为$y_j$。则回归系数和残差可通过如下方式计算

$$
\hat{\beta}=\frac{\sum_{1}^{N} x_{i} y_{i}}{\sum_{1}^{N} x_{i}^{2}} \tag{1}


$$

$$
r_i=y_i-x_i\hat{\beta}
$$

回归系数的计算很好理解，因为此处我们假设模型符合线性回归，则所有的$x_j和对应的y_j$都符合同一个线性表示，不考虑截距的情况下设这个线性表示为$y_j=\beta x_j$，则求回归系数变为

$$
\hat{\beta}=\frac{\sum_{1}^{N} \beta x_i^2 }{\sum_{1}^{N} x_i^2}
$$

注意这里的$\beta$是根据我们目前已有的观测值计算得出的，并不一定是解决这个问题的线性模型的真正的$\beta$，所以我们将它写为$\hat{\beta}$
为了叙述的方便，我们记$\mathbf{y}=(y_1,y_2...y_N)^T$，$\mathbf{x}=(x_1,x_2...x_N)^T$。则此时$\mathbf{x},\mathbf{y}$都是列向量（行向量转置）。定义

$$
\begin{aligned}
\langle\mathbf{x}, \mathbf{y}\rangle & =\sum_{i=1}^{N} x_{i} y_{i} \\
& =\mathbf{x}^{T} \mathbf{y}
\end{aligned}
$$

那么刚才\hat{\beta}的解可以写为

$$
\hat{\beta}={\frac{\langle\mathbf{x},\mathbf{y}\rangle}{\langle\mathbf{x},\mathbf{x}\rangle}},
$$

$$
\mathbf{r}=\mathbf{y}-\mathbf{x}{\hat{\boldsymbol{\beta}}}.
$$

对于输入数$\mathbf{x_1},\mathbf{x_2}...$ 等组成的输入矩阵$\mathbf{X}$，如果$\mathbf{x_1},\mathbf{x_2}...$ 俩俩正交，则最终得到的最小二乘系数矩阵$\mathbf{\beta}$中的第j列$\beta_j=<\mathbf{x_j},\mathbf{y}>/<\mathbf{x_j},\mathbf{x_j}>$，即最小二乘系数$\beta_j$的值仅与输入矩阵中的$\mathbf{x_j}$列有关而与其他列无关。这个结论从直观上是正确的。

接下来考虑有截距的情况，此时$y=\beta x+ b$。
