+++
title = "EX3"
date = 2022-03-30
categories = ["网络安全实验"]
draft = false
author = "Logic"
+++

## 实验过程 {#实验过程}


### 任务一 {#任务一}


#### 进行网络探测，收集目标网络存活的主机信息 {#进行网络探测-收集目标网络存活的主机信息}

使用nmap扫描192.168.1.0网段的存活主机，并探测其主机相关信息。能够探测到192.168.1.3，192.168.1.4，192.168.1.2(操作机)相关的信息192.168.1.3的主要信息如下

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/03309rVuq2.png" >}}

可见开启了21号ftp端口，运行的是vsftp3.0.2服务。
192.168.1.4的主要信息如下

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/0330wBYiWo.png" >}}


#### 使用嗅探工具进行嗅探 {#使用嗅探工具进行嗅探}

设置ettercap的嗅探模式为嗅探所有，监听网卡为默认eth0

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/0330QNuoeg.png" >}}

选择hosts-&gt;scan for hosts扫描网段内的主机，并将目标主机192.168.1.3设置为目标

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/0330mT6Hjp.png" >}}

选择Mitm-&gt;ARP poisoning进行ARP欺骗攻击。选择remote选项以进行双向欺骗。让目标机的数据包能够通过网关。这里要设置/proc/sys/net/ipv4/ip_forward的值为1

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/0330UhSAP0.png" >}}

出于安全考虑，Linux系统默认是禁止数据包转发的。所谓转发即当主机拥有多于一块的网卡时，其中一块收到数据包，根据数据包的目的ip地址将数据包发往本机另一块网卡，该网卡根据路由表继续发送数据包。这通常是路由器所要实现的功能。要让Linux系统具有路由转发功能，需要配置一个Linux的内核参数net.ipv4.ip_forward。这个参数指定了Linux系统当前对路由转发功能的支持情况；其值为0时表示禁止进行IP转发；如果是1,则说明IP转发功能已经打开。为了让操作机能嗅探到数据包，要打开路由转发功能。等待一段时间后为

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/0330K8fuEq.png" >}}

可见密码为ftp123


### 任务二 {#任务二}


#### 生成密码字典文件 {#生成密码字典文件}

使用crunch生成密码字典文件，从字符串“hacker +123456”中，随机选9个字符进行排列组合。生成格式为hacker@@@的密码字典

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/0330TYyp9j.png" >}}

查看password.txt文件

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/0330uCehZ1.png" >}}

可见已成功生成文件


#### 使用hydra爆破密码 {#使用hydra爆破密码}

查看hydra的ssh模块是否需要额外的参数

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/0330oRGjDs.png" >}}

可见不需要额外的参数，开始爆破，使用上一步生成的字典文件，-t 4表示爆破线程数为4，-f表示爆破成功一个后停止。

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/0330Hkecjm.png" >}}

可见爆破成功，密码为hacker123。


### 任务三 {#任务三}


#### 登陆目标机并获取key值 {#登陆目标机并获取key值}

使用得到的用户名和密码登陆目标机

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/0330sNBadY.png" >}}

查找1.key文件，并查看其中的内容

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/03303zYeUm.png" >}}

内容为ettercap


### 任务四 {#任务四}


#### 上传一句话木马 {#上传一句话木马}

打开网站，可以看到该网站使用了exponent CMS内容管理系统，搜索可知该内容管理系统在2.3.8版本以下存在文件上传漏洞。查看该漏洞的利用方式，可以写一个python脚本来利用该漏洞。上传一句话木马的部分如下

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/0330wBW6hZ.png" >}}

访问 `www.exponentcms.org/index.php?module=eventregistration&action=eventsCalendar` 然后右键查看网页源代码找到rel，这个rel就是大致的时间戳。

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/0330RfnERy.png" >}}

我们可以通过正则表达式获取到上传文件后的rel，获取rel的部分如下

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/0330Wozq3y.png" >}}

然后通过遍历尝试找到真正的文件访问路径，该部分如下：

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/033063L8nc.png" >}}

继续通过获得的文件访问路径来进行添加用户，将用户添加到管理组，并打印出机器的端口信息，该部分如下：

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/0330CCdd0X.png" >}}

整体执行流程放入main函数中

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/0330cmW0g7.png" >}}

建立一个一句话木马文件，命名为try.php

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/0330lXOfw3.png" >}}

将脚本和木马文件放入同一个文件夹下，执行脚本

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/0330jKcWm4.png" >}}

可见执行成功，且远程连接的端口为35155，连接主机

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/0330Jug7x3.png" >}}

连接成功。设置目标机C:\\2.key文件的可读权限，并查看该文件的具体内容。

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/0330zvZbN4.png" >}}

内容为

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/0330TLBqGm.png" >}}
