+++
title = "Ex1"
date = 2022-03-17
categories = ["网络安全实验"]
draft = false
author = "Logic"
+++

## 任务一 {#任务一}


### 扫描目标主机 {#扫描目标主机}

1.  ssh连接到kali实验机上
    ![](https://gitee.com/game-loader/picbase/raw/master/uPic/hkR81y.png)
    使用nmap扫描192.168.1.0网段的存活主机
    ![](https://gitee.com/game-loader/picbase/raw/master/uPic/ufarKj.png)
    可以看到通过Ping方法找到了网段中存活的三台主机，ip分别是192.168.1.2(操作机)，192.168.1.3(目标机)，192.168.1.4(目标机)，实际上-sP选项在默认情况下并非仅仅发送ICMP包，而是发送一个ICMP回声请求和一个TCP报文到80端口。如果非特权用户执行，就发送一个SYN报文 (用connect()系统调用)到目标机的80端口。 当特权用户扫描局域网上的目标机时，会发送ARP请求(-PR)。使用这种地毯式Ping可以快速查看网段中的存活主机。
2.  探测两台存活主机的端口和操作系统的相关信息可以使用-A选项分别对两台目标机进行全面扫描，目前这个选项启用了操作系统检测(-O) 和版本扫描(-sV)，以及其他一些后续添加的扫描功能。考虑到目标机和操作机在同一网段下且网络通畅，故可以使用-T4参数加速扫描过程。对第一台目标机(192.168.1.3)进行扫描的结果为：
    ![](https://gitee.com/game-loader/picbase/raw/master/uPic/UQTCaD.png)
    可以看到21(ftp),22(ssh),23(telnet),25(smtp),80(http)等端口对应的服务及其相关的信息，同时可以看到它通过smb脚本给出的对系统信息的总结为
    ![](https://gitee.com/game-loader/picbase/raw/master/uPic/f4v8mA.png)
    这里它推测目标机系统为Unix,且可能为Debian,但根据上面的一些服务对应的信息如ssl-cert,http server对应的信息也可以推测该系统为ubuntu，有关信息为
    ![](https://gitee.com/game-loader/picbase/raw/master/uPic/ZOtsTv.png)
    ![](https://gitee.com/game-loader/picbase/raw/master/uPic/2iyjCF.png)

    对第二台目标机(192.168.1.4)进行扫描的结果为：
    ![](https://gitee.com/game-loader/picbase/raw/master/uPic/Sam78z.png)
    可以看出目标主机开放了80端口，以及该端口对应的服务的信息。同时给出了对该目标机系统的猜测：
    ![](https://gitee.com/game-loader/picbase/raw/master/uPic/L7rfzY.png)


### 对目标机进行攻击 {#对目标机进行攻击}

在终端启动MSFCONSOLE，执行命令如下所示：
`root@simpleedu:~# msfconsole`
![](https://gitee.com/game-loader/picbase/raw/master/uPic/4lmgND.png)
搜索所有有效的vsftpd模块 `search vsftpd` ，结果如下：
![](https://gitee.com/game-loader/picbase/raw/master/uPic/ymQwlf.png)
可见有一个有效的可用模块，且效果评级为excellent，这里我们注意到它给出了这样的提示
![](https://gitee.com/game-loader/picbase/raw/master/uPic/259bV7.png)
为了加快后续的搜索速度，我们重建cache
![](https://gitee.com/game-loader/picbase/raw/master/uPic/HaBQdE.png)

![](https://gitee.com/game-loader/picbase/raw/master/uPic/dJ70PH.png)
根据该模块可以看出该模块适用于vsftpd v2.3.4，根据之前端口扫描的结果可知
![](https://gitee.com/game-loader/picbase/raw/master/uPic/yPCrsM.png)
目标机对应的ftp服务正是vsftpd v2.3.4，我们载入这个模块并查看其用法
![](https://gitee.com/game-loader/picbase/raw/master/uPic/a8fvRd.png)
可见需要设定目标机地址和端口，我们设定好目标机地址为192.168.1.3
![](https://gitee.com/game-loader/picbase/raw/master/uPic/HwGZYT.png)
查找该模块可用的payload并载入payload
![](https://gitee.com/game-loader/picbase/raw/master/uPic/kPpuxZ.png)
进行攻击，并查找扩展名为key的所有文件并查看1.key文件的内容
![](https://gitee.com/game-loader/picbase/raw/master/uPic/7RiZZk.png)
可知1.key中的内容为Metasploit


## 任务二 {#任务二}


### 扫描目标机网站目录结构 {#扫描目标机网站目录结构}

`nikto -h http://192.168.1.4 -c all`
-h用于设定目标主机，-c用于扫描目录，这里为了后续实验需要进入图形界面环境，我们切换至图形界面并打开图形界面如下：

```shell
systemctl set-default graphical.target
init 5
```

然后用Vnc viewer连接至操作机即可。扫描目录结构的结果如下：
![](https://gitee.com/game-loader/picbase/raw/master/uPic/Z1x2HN.png)
由倒数第三行可知在存在admin/login.php文件即为该网站的后台管理界面。浏览器查看管理界面如下：
![](https://gitee.com/game-loader/picbase/raw/master/uPic/OEmOYF.png)


### 创建字典文件 {#创建字典文件}

使用crunch命令，生成一个每行以admin开头加3位随机数字共8位字符串长度的字典文件。
![](https://gitee.com/game-loader/picbase/raw/master/uPic/DIMaJ6.png)
使用-t指定生成格式，用%代表数字，最小长度和最大长度都是8，这样会生成以admin开头加三位数字的所有组合，-o指定输出到/root/password.txt文件中，查看该文件可知
![](https://gitee.com/game-loader/picbase/raw/master/uPic/iL3v24.png)
可见已成功生成所有组合。


### 破解密码 {#破解密码}

在操作机上进入网站后台管理界面，并设置代理。
![](https://gitee.com/game-loader/picbase/raw/master/uPic/suIdxo.png)
打开burpsuit，在管理界面输入用户名admin和密码aaccdd(任意的)，登入，此时burpsuit会拦截对应的数据包。可以查看到提交的用户名和密码
![](https://gitee.com/game-loader/picbase/raw/master/uPic/xPcjYA.png)
在此空白处右键，选择[send to intruder]
![](https://gitee.com/game-loader/picbase/raw/master/uPic/cF2DYG.png)
点击主菜单的[intruder]可见已经根据请求将目标主机ip和端口设置好了
![](https://gitee.com/game-loader/picbase/raw/master/uPic/nh24cl.png)
在Intruder-Position中设置，将自动设置的position[Clear]掉，然后在请求中password的地方点击[Add]添加position,设置攻击类型为[sniper],这种模式会依次尝试你设置变量的地方，先对一个位置进行尝试，其他位置不变，然后再改变下一个位置。我们只有一个爆破位置，故使用该模式即可。
![](https://gitee.com/game-loader/picbase/raw/master/uPic/3J46dc.png)
在Intruder-Payloads中设置攻击载荷，选择[load]，将刚才生成的password.txt中的数据加载进字典列表
![](https://gitee.com/game-loader/picbase/raw/master/uPic/wJNx9T.png)
点击start-attack开始攻击
![](https://gitee.com/game-loader/picbase/raw/master/uPic/HKeFji.png)
等待爆破完成点击[length]对结果进行排序，只有一个结果长度与其他不同，可查看其对应的response
![](https://gitee.com/game-loader/picbase/raw/master/uPic/lW0GpG.png)
状态码为302表示网页被重定向，很可能是登陆成功，在response中没有明显的成功信息。我们关掉burpsuit的包拦截，并使用这个密码(admin452)尝试登陆。
![](https://gitee.com/game-loader/picbase/raw/master/uPic/p2PyTr.png)
登陆成功


## 任务三 {#任务三}

Webshell是黑客经常使用的一种恶意脚本，其目的是获得对服务器的执行操作权限，常见的webshell编写语言为asp、jsp和php。本次实验中使用php的webshell.


### 添加webshell {#添加webshell}

tag本是网站的一个插件，可以用于使用php编程，方便执行某些功能，但因system函数可以执行终端指令，故也可被用于执行一些恶意指令。首先利用这一特性查看当前网站在服务器上的目录
![](https://gitee.com/game-loader/picbase/raw/master/uPic/fef9Qg.png)
可知当前网站的管理页面目录为 `C:\phpStudy\WWW\admin` ，故可以使用echo指令向服务器根目录写入一个php文件，内容为一句话木马，即 `eval($_GET['shell'])` ，其中eval在php中为将参数字符串作为代码执行，$_GET变量用于收集来自 method="get" 的表单中的值，我们将其写入到admin文件夹下，如下：
![](https://gitee.com/game-loader/picbase/raw/master/uPic/HzulTj.png)
点击apply,run之后系统就会运行echo指令将一句话木马写入hack.php文件中，此时该文件的功能与1.php类似，都可以执行通过GET传递的命令。这里需要注意的地方是echo写入的字符串中&lt; &gt;需要转义，故应写为^&lt; 和 ^&gt;，且system函数的参数字符串中的\\和'也需要转义，应写为\\\\和\\'。此时我们尝试通过写入的php文件执行指令，可得：
![](https://gitee.com/game-loader/picbase/raw/master/uPic/POUdsV.png)
可见已成功获得一个webshell。


### 获取端口号 {#获取端口号}

执行指令 `tasklist /svc | findstr "TermService"` (tasklist会列出所有进程及其使用的服务）获得远程桌面服务对应的进程PID
![](https://gitee.com/game-loader/picbase/raw/master/uPic/6OVvbV.png)
然后通过 `netstat -ano | findstr "2140"` (netstat -a显示所有活跃或不活跃的端口及连接，-o显示进程PID便于查找)找到该PID对应的进程使用的端口号
![](https://gitee.com/game-loader/picbase/raw/master/uPic/6ABKkz.png)
可知端口号为45565


### 另外一种方法 {#另外一种方法}

除了利用命令写入文件，我们注意到网站后台提供了上传文件的功能，因此我们可以直接使用kali中的weevely生成一个有webshell的php文件并上传到网站后台，然后利用weevely连接。过程如下生成webshell
![](https://gitee.com/game-loader/picbase/raw/master/uPic/zMurto.png)
上传文件,将生成的webshell.php拖拽到此处
![](https://gitee.com/game-loader/picbase/raw/master/uPic/amzNbb.png)
可以看到上传后的文件夹为uploads
![](https://gitee.com/game-loader/picbase/raw/master/uPic/Ih5Ty4.png)
利用weevely连接到webshell
![](https://gitee.com/game-loader/picbase/raw/master/uPic/01bcUK.png)
可见连接成功


## 任务四 {#任务四}


### 添加用户 {#添加用户}

执行命令使用任务三中利用weevely获取到的shell，通过网页webshell执行的命令相同，只是需要将命令放入system()中通过GET请求提交即可，有双引号要转义，这里用weevely的shell方便查看格式化输出的结果。
![](https://gitee.com/game-loader/picbase/raw/master/uPic/OHdanX.png)
查看本机的所有用户
![](https://gitee.com/game-loader/picbase/raw/master/uPic/AJ1B3I.png)
可见添加成功


### 将用户加入管理员组 {#将用户加入管理员组}

![](https://gitee.com/game-loader/picbase/raw/master/uPic/log5Og.png)
查看管理员组信息
![](https://gitee.com/game-loader/picbase/raw/master/uPic/NFXuN9.png)
可见添加成功


### 以hacker用户身份登陆目标机 {#以hacker用户身份登陆目标机}

使用kali的rdesktop连接至目标机
![](https://gitee.com/game-loader/picbase/raw/master/uPic/NZXOJN.png)


### 更改2.key的权限 {#更改2-dot-key的权限}

右击属性-&gt;安全-&gt;高级-&gt;更改文件所有者为hacker，如下：
![](https://gitee.com/game-loader/picbase/raw/master/uPic/Tz5dBl.png)
给hacker赋予对文件的完全控制权限
![](https://gitee.com/game-loader/picbase/raw/master/uPic/L5uj74.png)
![](https://gitee.com/game-loader/picbase/raw/master/uPic/TiIeSd.png)
用记事本打开文件，可以看到内容为zenmap
![](https://gitee.com/game-loader/picbase/raw/master/uPic/KVOexB.png)
