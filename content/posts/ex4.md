+++
title = "Ex4"
date = 2022-04-06
categories = ["网络安全实验"]
draft = false
author = "Logic"
+++

## 实验过程 {#实验过程}


### 任务一 {#任务一}

双击桌面Xshell5图标，在弹出的界面登陆主机192.168.1.11和192.168.2.11这两台主机.

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/0405TxkNea.png" >}}

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/04050wlYRk.png" >}}

分别修改主机名：

```shell
hostnamectl set-hostname vpn1
hostnamectl set-hostname vpn2
```

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/04050crxmG.png" >}}

重新登陆两台主机后如下图

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/0405oi9VQ5.png" >}}

vpn1和vpn2主机分别加载gre内核模块并检查

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/0405xgQFvC.png" >}}

> 通用路由封装或gre是一种协议，用于将使用一个路由协议的数据包封装在另一协议的数据包中。gre是在网络上建立直接点对点连接的一种方法，目的是简化单独网络之间的连接。它适用于各种网络层协议。将数据包封装在其他数据包中称为“隧道”。gre隧道通常配置在两个路由器之间，每个路由器的作用好比隧道的一端。路由器设置为彼此直接发送和接收 gre数据包。两个路由器之间的任何路由器都不会打开封装的数据包；它们仅引用封装数据包外层的标头进行转发。
> ip_gre内核模块是gre通过IPv4隧道的驱动程序

vpn1创建一个GRE类型隧道设备gre1, 并设置对端IP为192.168.2.11。
ip指令用于设置网络设备，用于替代ifconfig命令，ip tunnel用于建立隧道。mode表示模式为gre，设置远端和本地端。ip a用于显示ip地址。

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/040510B3Cj.png" >}}

启动gre1并分配ip地址10.10.10.1，检测是否添加并启动

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/0405AWgwKv.png" >}}

类似的，对vpn2主机进行相应的配置

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/0405puFW5b.png" >}}

测试是否连通

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/0405lNqMVV.png" >}}

卸载gre模块

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/0405ICPgN1.png" >}}


### 任务二 {#任务二}

查看openssl命令基本帮助

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/04052LODMa.png" >}}

> openssl是一个强大的安全套接字层密码库，囊括主要的密码算法、常用的密钥和证书封装管理功能及SSL协议，并提供丰富的应用程序供测试或其它目的使用。 openssl整个软件包大概可以分成三个主要的功能部分：SSL协议库、应用程序以及密码算法库。openssl的目录结构自然也是围绕这三个功能部分进行规划的。基本功能有：主要的密码算法（MD5、SHA、DH、BASE64等等）、常用的密钥和证书封装管理功能以及SSL协议，并提供了丰富的应用程序供测试或其它目的使用。

产生RSA私钥

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/0405cvz7dz.png" >}}

这样就生成了2048位的RSA私钥接下来利用生成的私钥生成对应的公钥，查看openssl rsa对应的用法

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/0405trT262.png" >}}

则可见应将私钥文件作为输入文件，选择输出公钥并指定公钥文件。

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/0405Fu4P3g.png" >}}

接下来生成RSA含密码(使用AES-256加密)的公私钥文件生成私钥时只需要指定使用AES-256进行加密并输入相应的密码即可，这里把密码设定为simple。

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/0405AI9uzB.png" >}}

同样生成对应的公钥文件，并查看生成的全部秘钥文件

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/0405FIuJD2.png" >}}

接下来尝试将使用AES-256加密的文件和未使用其加密的文件进行转换

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/0405JpX1OG.png" >}}

生成自签名证书

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/0405G4S6Rd.png" >}}

也可使用已有的私钥生成自签名证书，如下

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/0405j0AtId.png" >}}

生成私钥及csr签名请求文件(生成过程中输错了一次密码故产生了错误)

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/0405qaSIwD.png" >}}


### 实验三 {#实验三}

在vpn1机器安装openvpn并验证

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/0406ZbPz2i.png" >}}

拷贝模版配置文件到openvpn的配置文件目录下

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/0406Zvf560.png" >}}

修改openvpn服务端的配置文件server.conf。指定使用TCP协议。将UDP注释掉，不需要配置DNS因为无法连通外网。

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/0406w79zzB.png" >}}

将openvpn运行的用户设为nobody，用户组设为nobody，可降低openvpn的权限，增强其安全性。

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/0406VNz4um.png" >}}

将explicit-exit-notify 1注释掉以关闭显式退出提示

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/0406PZcWZK.png" >}}

安装密钥生成软件easy-rsa

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/04066Yi8ya.png" >}}

将easy-rsa拷贝到openvpn文件夹下

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/0406Yz2aST.png" >}}

配置生成证书的环境变量并使之生效

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/0406HbkqrQ.png" >}}

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/0406UJoutO.png" >}}

删除之前的keys，并重新生成

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/0406zFTeXR.png" >}}

创建通用名(common name)为”server”的证书文件，并设置密码为simple123。

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/04060gd4O8.png" >}}

生成防攻击的key文件，可以防止DDos攻击，UDP淹没等恶意攻击。其实从本质上说就是一个静态密钥文件，使用静态密钥文件进行连接相比于证书方式进行连接不需要TLS握手，不易被干扰。

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/0406ss9qjC.png" >}}

生成客户端使用的密钥文件

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/0406hZKHfH.png" >}}

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/0406iIdFPn.png" >}}

将全部的密钥认证文件拷贝到openvpn的配置文件目录下

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/0406iMTcL5.png" >}}

创建一个通用名(common name)为 client的客户端证书

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/04062WlMTb.png" >}}

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/04064tfgcG.png" >}}

启动openvpn服务并设置为开机自启动，随后查看服务的状态并检查服务对应的端口是否处于监听状态

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/0406DR4Pz7.png" >}}

服务端配置完成后进行客户端的登陆测试，首先在客户端安装openvpn

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/0406wi0th8.png" >}}

将服务端生成的对应的ca证书和客户端密钥以及连接使用的静态密钥文件通过scp拷贝到客户端

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/04065x5iam.png" >}}

配置客户端的配置文件，进行连接前的准备，设定配置文件中连接的目标机器ip，端口，需要的密钥文件，加密方式，重新测试，预设log等级等，这一部分与服务端配置上大体保持一致。

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/0406OnB3Ut.png" >}}

启动openvpn客户端并挂后台运行，并可实时查看其日志。

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/04067UF246.png" >}}

查看网卡信息，查看vpn通道的建立情况

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/0406OOIR6B.png" >}}

测试是否可用，这里ping 10.8.0.1是该ip与vpn连接的目标机ip 10.8.0.5处于同一网段下，故可以连通。

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/0406CSYhPm.png" >}}

在vpn1机器上对openvpn nat进行配置，使用的工具是iptables，iptables是一款管理员工具，用于IPv4包过滤和NAT转换相关的功能，其与netfilter结合可以组成Linux平台下的包过滤防火墙。-t表示指定规则对应的表，-A表示向指定的规则链末尾增加一条规则，-s则表示源地址，-j表示对应的动作，这里MASQUERADE表示伪装，即用转换后的地址代替之前的地址。

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/0406Ju7uf6.png" >}}

在vpn2机器上访问百度进行测试

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/0406Ov4KJw.png" >}}

在vpn1机器上查看对应规则下的数据包

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/0406CzChHs.png" >}}

在两台机器上关闭openvpn服务

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/0406GzQ0Zp.png" >}}


### 任务四 {#任务四}

对于vpn1和vpn2，调整内核参数，开启数据转发，关闭icmp重定向并使之生效。

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/0406prsHtf.png" >}}

在vpn1和vpn2上安装openswan，libreswan并验证安装

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/0406lbe3ZZ.png" >}}

启动服务并检查是否正常。

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/0406wvzdK8.png" >}}

可见服务正常启动。查看openswan监听的端口

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/0406XvrAGA.png" >}}

可见在监听500和4500端口，其中500是用来IKE密钥交换协商，4500的NAT-T是nat穿透的。配置ipsecVPN配置，首先使用PSK方式进行配置，修改ipsec.conf文件，在末尾添加如下

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/0406Ez81iq.png" >}}

修改vpn1和vpn2的密码配置文件，在配置文件末尾添加本机ip，允许任意ip连接，密码为123

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/04069RcB9n.png" >}}

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/04064KOqTu.png" >}}

重新启动服务并验证

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/0406CS0F8v.png" >}}

可见连接成功。在两台机器上搭建内网网络来进行连通性测试，在vpn1上搭建虚拟网络10.0.0.1/24。在vpn2上搭建虚拟网络10.0.1.1/24，搭建完成后如下

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/0406yV3W14.png" >}}

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/040610jGiD.png" >}}

在vpn1上ping测试，可以成功

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/0406N1qUiw.png" >}}

现在使用数字签名模式认证，在vpn1和vpn2上分别生成一个新的RSA密钥对，查看ipsec用法

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/0406y6Qbwz.png" >}}

其中newhostkey用于生成一个新的rsa认证密钥。我们可以使用 man ipsec newhostkey查看其用法，如下

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/0406kswcXM.png" >}}

使用newhostkey生成一个新的认证密钥，为

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/0406Potr2b.png" >}}

记下此时的Key为73c6cb9c1166c10f4d242d1b8b2ff602fba43c54
查看本机的认证密钥，使用showhostkey，同样可用man ipsec shwohostkey查看其用法

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/0406lrpABy.png" >}}

查看本机密钥为

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/0406SgTLDY.png" >}}

为了方便将密钥放入ipsec配置文件中我们将密钥输出到文件中

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/0406Aib0k5.png" >}}

再通过vim同时打开两个文件复制密钥到ipsec.conf中，将文件发至vpn2并在vpn2上进行同样操作，最终如下

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/0406Z9eVPf.png" >}}

重启ipsec服务并打开连接，显示IPsec SA established tunnel mode则连接成功

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/0406dfHELc.png" >}}

在vpn1上ping测试，可以成功

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/0406kPecJY.png" >}}

清除内网，停止服务

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/0406Canxn6.png" >}}


### 任务五 {#任务五}

在vpn1和vpn2分别安装openvswitch并启动服务

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/0406fSQevV.png" >}}

可见安装并启动成功在vpn1上添加名为br0的网桥并给网桥分配一个ip

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/0406HizoA5.png" >}}

可见添加及分配ip成功在vpn2上同样添加br0网桥并分配ip

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/0406KejOfw.png" >}}

在VPN1上设置VXLAN，远端ip设置为VPN2能对外通信的br0的ip。

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/0406YJoFTn.png" >}}

在VPN2上进行同样的设置

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/0406cqWPh4.png" >}}

在VPN1上ping 10.1.0.2

{{< figure src="https://gcore.jsdelivr.net/gh/game-loader/picbase@master/uPic/0406qblPEZ.png" >}}

可以Ping通说明连接成功
