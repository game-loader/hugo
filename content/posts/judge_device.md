+++
title = "判断linux下音频输入对应的文件"
date = 2022-03-26
categories = ["bpf"]
draft = false
author = "Logic"
+++

## 判断音频输入 {#判断音频输入}

摄像头对应的设备属实是非常好找，但是麦克风对应的设备可就没那么容易了。本以为就应该是什么 `/dev/audio` 这样的东西，结果一看/dev文件夹，看不出任何相关的存在。查找资料可以找到如下一张表格[^fn:1]

| ALSA設備         | OSS設備                  | 次設備號 |
|----------------|------------------------|------|
| /dev/snd/pcmC0D0 | /dev/audio0（/dev /audio） | 4    |
| /dev/snd/pcmC0D0 | /dev/dsp0（/dev/dsp）    | 3    |
| /dev/snd/pcmC0D1 | /dev/adsp（/dev/adsp）   | 12   |
| /dev/snd/pcmC1D0 | /dev/audio1              | 20   |
| /dev/snd/pcmC1D0 | /dev/dsp1                | 19   |
| /dev/snd/pcmC1D1 | /dev/adsp1               | 28   |
| /dev/snd/pcmC2D0 | /dev/audio2              | 36   |
| /dev/snd/pcmC2D0 | /dev/dsp2                | 35   |
| /dev/snd/pcmC2D1 | /dev/adsp2               | 44   |

可见在使用alsa作为linux声音的驱动程序后，alsa会创建一系列设备映射，使用alsa设备就相当于使用对应的oss设备，即麦克风。

[^fn:1]: The link is [理解和使用alsa配置](https://www.twblogs.net/a/5b7f76a12b717767c6afc8e2)
